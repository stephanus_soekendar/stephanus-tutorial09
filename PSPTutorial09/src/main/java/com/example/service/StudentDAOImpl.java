package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.dao.StudentDAO;
import com.example.model.StudentModel;

@Service
public class StudentDAOImpl implements StudentDAO
{
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	
	@Autowired
	private RestTemplate restTemplate = this.restTemplate();
	
	@Override
	public StudentModel selectStudent (String npm)
	{
		StudentModel student =
			restTemplate.getForObject(
			"http://localhost:9090/rest/student/view/"+npm,
			StudentModel.class);
		return student;
	}
	
	@Override
	public List<StudentModel> selectAllStudents ()
	{
		return null;
	}
}

