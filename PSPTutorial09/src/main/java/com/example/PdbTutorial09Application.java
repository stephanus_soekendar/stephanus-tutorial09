package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PdbTutorial09Application
{

    public static void main (String[] args)
    {
        SpringApplication.run (PdbTutorial09Application.class, args);
    }
}
